
public class Sheep {

	enum Animal {
		sheep, goat
	};

	public static void main(String[] param) {
		// for debugging
	}

	public static void reorder(Animal[] animals) {
		// TODO!!! Your program here

		int viimane = animals.length - 1;
		int esimene = 0;
		/*
		 * Allj�rgnev lahendusk�igu valmimisel on v�etud eeskuju Hoare
		 * sorteerimise algoritmist. Oluline oli sorteerimise kiirus ning hetkel
		 * teadaolevalt on Hoare sorteerimise algoritm k�ige kiirem, on veel
		 * teisi sorteerimise viise - mergersort ning insertsort aga oma
		 * kiiruselt j��vad on Hoarele alla.
		 *
		 * T��s on kasutatud Hoare algoritmi ideed,
		 * https://en.wikipedia.org/wiki/Quicksort
		 * 
		 */
		while (esimene < viimane) {
			if (animals[esimene] == Animal.goat) {
				esimene++;
			} else if (animals[viimane] == Animal.sheep) {
				viimane--;
			} else {
				Animal vahetabel = animals[esimene];
				animals[esimene] = animals[viimane];
				animals[viimane] = vahetabel;
			}
		}

		return;

	}
}
